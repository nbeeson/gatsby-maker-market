import React from "react";
import propTypes from "prop-types";
import { graphql, StaticQuery } from "gatsby";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from '@material-ui/core/styles';
import PreviewCompatibleImage from './PreviewCompatibleImage'


const ProductList = ({ data }) => (
  <div>
    {console.log(data.allMarkdownRemark.edges)}
    {data.allMarkdownRemark.edges.map((product) => (
      <Card style= {{ padding:5,margin:20}}>
        <CardActionArea>
          <div>
          <PreviewCompatibleImage
                        imageInfo={{
                          image: product.node.frontmatter["image"],
                          alt: `featured image thumbnail for post ${product.node.frontmatter["title"]}`,
                        }}
                      />
            <Typography gutterBottom variant="h5" component="h2">
              {product.node.frontmatter["title"]}
            </Typography>
            <p>{product.node.frontmatter["description"]}</p>
          </div>
        </CardActionArea>
        <CardActions>
        <Button size="small" color="primary">
          Share
        </Button>
        <Button size="small" color="primary">
          Learn More
        </Button>
      </CardActions>
      </Card>
    ))}
  </div>
);

export default () => (
  <StaticQuery
    query={graphql`
      query ProductQuery {
        allMarkdownRemark(
          filter: { frontmatter: { templateKey: { eq: "product-post" } } }
        ) {
          edges {
            node {
              frontmatter {
                title
                templateKey
                description
                tags
                image {
                  childImageSharp {
                    fluid(maxWidth: 120, quality: 100) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={(data) => <ProductList data={data} />}
  />
);

// - name: "product"
// label: "Blog"
// folder: "src/products"
// create: true
// slug: "{{year}}-{{month}}-{{day}}-{{slug}}"
// fields:
//   - {label: "Template Key", name: "templateKey", widget: "hidden", default: "product-type"}
//   - {label: "Title", name: "title", widget: "string"}
//   - {label: "Description", name: "description", widget: "text"}
//   - {label: "Image", name: "image", widget: image}
//   - {label: "Body", name: "body", widget: "markdown"}
//   - {label: "Tags", name: "tags", widget: "list"}
