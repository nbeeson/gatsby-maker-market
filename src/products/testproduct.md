---
templateKey: product-type
title: Test Product
image: /img/chemex.jpg
description: Brewing with a Chemex probably seems like a complicated, time-consuming ordeal, but once you get used to the process, it becomes a soothing ritual that's worth the effort every time.
tags:
  - brewing
  - chemex
---